# Swagger\Server\Api\ServiceApiInterface

All URIs are relative to *https://virtserver.swaggerhub.com/Clinic_T/Clinic_API/1.0.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**changeService**](ServiceApiInterface.md#changeService) | **PUT** /service/change | Change service
[**getService**](ServiceApiInterface.md#getService) | **GET** /service/get | Get service


## Service Declaration
```yaml
# src/Acme/MyBundle/Resources/services.yml
services:
    # ...
    acme.my_bundle.api.service:
        class: Acme\MyBundle\Api\ServiceApi
        tags:
            - { name: "swagger_server.api", api: "service" }
    # ...
```

## **changeService**
> Swagger\Server\Model\Service changeService($token, $body)

Change service

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/ServiceApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\ServiceApiInterface;

class ServiceApi implements ServiceApiInterface
{

    // ...

    /**
     * Implementation of ServiceApiInterface#changeService
     */
    public function changeService($token, array $body)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **token** | **string**| Token logged user |
 **body** | [**Swagger\Server\Model\Service**](../Model/Service.md)|  |

### Return type

[**Swagger\Server\Model\Service**](../Model/Service.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **getService**
> Swagger\Server\Model\Service getService($token)

Get service

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/ServiceApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\ServiceApiInterface;

class ServiceApi implements ServiceApiInterface
{

    // ...

    /**
     * Implementation of ServiceApiInterface#getService
     */
    public function getService($token)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **token** | **string**| Token logged user |

### Return type

[**Swagger\Server\Model\Service**](../Model/Service.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

