# Swagger\Server\Api\ClientApiInterface

All URIs are relative to *https://virtserver.swaggerhub.com/Clinic_T/Clinic_API/1.0.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createCommentClient**](ClientApiInterface.md#createCommentClient) | **POST** /client/createComment | Create comment
[**editClient**](ClientApiInterface.md#editClient) | **PUT** /client/EditInfo | Edit info user
[**entryClient**](ClientApiInterface.md#entryClient) | **POST** /client/Entry | Entry with a doctor
[**getClinics**](ClientApiInterface.md#getClinics) | **GET** /client/getClinic | Get Clinics
[**getDoctors**](ClientApiInterface.md#getDoctors) | **GET** /client/getDoctors | Get doctors
[**logOutCleint**](ClientApiInterface.md#logOutCleint) | **GET** /client/logout | LogOut Client
[**loginClient**](ClientApiInterface.md#loginClient) | **GET** /client/login | Login Client
[**signUpClient**](ClientApiInterface.md#signUpClient) | **POST** /client/signUp | Sign Up new Client


## Service Declaration
```yaml
# src/Acme/MyBundle/Resources/services.yml
services:
    # ...
    acme.my_bundle.api.client:
        class: Acme\MyBundle\Api\ClientApi
        tags:
            - { name: "swagger_server.api", api: "client" }
    # ...
```

## **createCommentClient**
> createCommentClient($body)

Create comment

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/ClientApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\ClientApiInterface;

class ClientApi implements ClientApiInterface
{

    // ...

    /**
     * Implementation of ClientApiInterface#createCommentClient
     */
    public function createCommentClient(array $body)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | **array**|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **editClient**
> string editClient($body)

Edit info user

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/ClientApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\ClientApiInterface;

class ClientApi implements ClientApiInterface
{

    // ...

    /**
     * Implementation of ClientApiInterface#editClient
     */
    public function editClient(array $body)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | **array**|  |

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **entryClient**
> string entryClient($date, $body)

Entry with a doctor

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/ClientApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\ClientApiInterface;

class ClientApi implements ClientApiInterface
{

    // ...

    /**
     * Implementation of ClientApiInterface#entryClient
     */
    public function entryClient($date, array $body)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **date** | **string**|  |
 **body** | **array**|  |

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **getClinics**
> Swagger\Server\Model\Clinic getClinics($city, $type)

Get Clinics

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/ClientApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\ClientApiInterface;

class ClientApi implements ClientApiInterface
{

    // ...

    /**
     * Implementation of ClientApiInterface#getClinics
     */
    public function getClinics($city, $type)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **city** | **string**|  |
 **type** | **string**|  |

### Return type

[**Swagger\Server\Model\Clinic**](../Model/Clinic.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **getDoctors**
> Swagger\Server\Model\Employee getDoctors($filter, $body)

Get doctors

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/ClientApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\ClientApiInterface;

class ClientApi implements ClientApiInterface
{

    // ...

    /**
     * Implementation of ClientApiInterface#getDoctors
     */
    public function getDoctors($filter, array $body)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**|  |
 **body** | **array**|  |

### Return type

[**Swagger\Server\Model\Employee**](../Model/Employee.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **logOutCleint**
> string logOutCleint()

LogOut Client

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/ClientApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\ClientApiInterface;

class ClientApi implements ClientApiInterface
{

    // ...

    /**
     * Implementation of ClientApiInterface#logOutCleint
     */
    public function logOutCleint()
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **loginClient**
> string loginClient($username, $password)

Login Client

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/ClientApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\ClientApiInterface;

class ClientApi implements ClientApiInterface
{

    // ...

    /**
     * Implementation of ClientApiInterface#loginClient
     */
    public function loginClient($username, $password)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| The client name for login |
 **password** | **string**| The password for autorizate |

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **signUpClient**
> Swagger\Server\Model\Client signUpClient($body)

Sign Up new Client

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/ClientApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\ClientApiInterface;

class ClientApi implements ClientApiInterface
{

    // ...

    /**
     * Implementation of ClientApiInterface#signUpClient
     */
    public function signUpClient(array $body)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | **array**|  |

### Return type

[**Swagger\Server\Model\Client**](../Model/Client.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

