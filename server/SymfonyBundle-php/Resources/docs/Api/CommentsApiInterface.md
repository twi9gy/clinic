# Swagger\Server\Api\CommentsApiInterface

All URIs are relative to *https://virtserver.swaggerhub.com/Clinic_T/Clinic_API/1.0.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteComment**](CommentsApiInterface.md#deleteComment) | **DELETE** /comments/delete | Delete comment
[**getComments**](CommentsApiInterface.md#getComments) | **GET** /comments/get | Get comments


## Service Declaration
```yaml
# src/Acme/MyBundle/Resources/services.yml
services:
    # ...
    acme.my_bundle.api.comments:
        class: Acme\MyBundle\Api\CommentsApi
        tags:
            - { name: "swagger_server.api", api: "comments" }
    # ...
```

## **deleteComment**
> string deleteComment($commentId, $token)

Delete comment

This can only be done by the logged in user.

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/CommentsApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\CommentsApiInterface;

class CommentsApi implements CommentsApiInterface
{

    // ...

    /**
     * Implementation of CommentsApiInterface#deleteComment
     */
    public function deleteComment($commentId, $token)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **commentId** | **string**|  |
 **token** | **string**| Token logged user |

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **getComments**
> Swagger\Server\Model\Comment getComments($token)

Get comments

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/CommentsApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\CommentsApiInterface;

class CommentsApi implements CommentsApiInterface
{

    // ...

    /**
     * Implementation of CommentsApiInterface#getComments
     */
    public function getComments($token)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **token** | **string**| Token logged user |

### Return type

[**Swagger\Server\Model\Comment**](../Model/Comment.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

