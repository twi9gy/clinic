# Swagger\Server\Api\EmployeeApiInterface

All URIs are relative to *https://virtserver.swaggerhub.com/Clinic_T/Clinic_API/1.0.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createEmployee**](EmployeeApiInterface.md#createEmployee) | **POST** /employee/create | Create employee
[**deleteEmployee**](EmployeeApiInterface.md#deleteEmployee) | **DELETE** /employee/delete | Update employee
[**getEmployee**](EmployeeApiInterface.md#getEmployee) | **GET** /employee/get | Get employee
[**updateEmployee**](EmployeeApiInterface.md#updateEmployee) | **PUT** /employee/update | Update employee


## Service Declaration
```yaml
# src/Acme/MyBundle/Resources/services.yml
services:
    # ...
    acme.my_bundle.api.employee:
        class: Acme\MyBundle\Api\EmployeeApi
        tags:
            - { name: "swagger_server.api", api: "employee" }
    # ...
```

## **createEmployee**
> string createEmployee($body)

Create employee

This can only be done by the logged in user.

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/EmployeeApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\EmployeeApiInterface;

class EmployeeApi implements EmployeeApiInterface
{

    // ...

    /**
     * Implementation of EmployeeApiInterface#createEmployee
     */
    public function createEmployee(Employee $body)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Swagger\Server\Model\Employee**](../Model/Employee.md)|  |

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **deleteEmployee**
> string deleteEmployee($employeeId, $token)

Update employee

This can only be done by the logged in user.

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/EmployeeApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\EmployeeApiInterface;

class EmployeeApi implements EmployeeApiInterface
{

    // ...

    /**
     * Implementation of EmployeeApiInterface#deleteEmployee
     */
    public function deleteEmployee($employeeId, $token)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **employeeId** | **string**|  |
 **token** | **string**| Token logged user |

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **getEmployee**
> Swagger\Server\Model\Employee getEmployee($token, $filter)

Get employee

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/EmployeeApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\EmployeeApiInterface;

class EmployeeApi implements EmployeeApiInterface
{

    // ...

    /**
     * Implementation of EmployeeApiInterface#getEmployee
     */
    public function getEmployee($token, $filter)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **token** | **string**| Token logged user |
 **filter** | **string**|  |

### Return type

[**Swagger\Server\Model\Employee**](../Model/Employee.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **updateEmployee**
> string updateEmployee($body)

Update employee

This can only be done by the logged in user.

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/EmployeeApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\EmployeeApiInterface;

class EmployeeApi implements EmployeeApiInterface
{

    // ...

    /**
     * Implementation of EmployeeApiInterface#updateEmployee
     */
    public function updateEmployee(Employee $body)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Swagger\Server\Model\Employee**](../Model/Employee.md)|  |

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

