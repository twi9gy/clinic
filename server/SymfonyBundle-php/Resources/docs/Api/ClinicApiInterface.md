# Swagger\Server\Api\ClinicApiInterface

All URIs are relative to *https://virtserver.swaggerhub.com/Clinic_T/Clinic_API/1.0.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**editInfo**](ClinicApiInterface.md#editInfo) | **PUT** /clinic/editInfo | Eedit main info for clinic
[**logOutClinic**](ClinicApiInterface.md#logOutClinic) | **GET** /clinic/logout | Logs out current logged in user session
[**login**](ClinicApiInterface.md#login) | **GET** /clinic/login | Login user clinic
[**signUp**](ClinicApiInterface.md#signUp) | **POST** /clinic/signUp | Create new user
[**updatePrivateOfiice**](ClinicApiInterface.md#updatePrivateOfiice) | **PUT** /clinic/edit_privateOffice | Update user info
[**uploadImg**](ClinicApiInterface.md#uploadImg) | **PUT** /clinic/uploadImg | Upload img on main page clinic


## Service Declaration
```yaml
# src/Acme/MyBundle/Resources/services.yml
services:
    # ...
    acme.my_bundle.api.clinic:
        class: Acme\MyBundle\Api\ClinicApi
        tags:
            - { name: "swagger_server.api", api: "clinic" }
    # ...
```

## **editInfo**
> string editInfo($token, $body)

Eedit main info for clinic

This can only be done by the logged in user.

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/ClinicApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\ClinicApiInterface;

class ClinicApi implements ClinicApiInterface
{

    // ...

    /**
     * Implementation of ClinicApiInterface#editInfo
     */
    public function editInfo($token, Clinic $body)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **token** | **string**|  |
 **body** | [**Swagger\Server\Model\Clinic**](../Model/Clinic.md)|  |

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **logOutClinic**
> string logOutClinic()

Logs out current logged in user session

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/ClinicApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\ClinicApiInterface;

class ClinicApi implements ClinicApiInterface
{

    // ...

    /**
     * Implementation of ClinicApiInterface#logOutClinic
     */
    public function logOutClinic()
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **login**
> string login($username, $password)

Login user clinic

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/ClinicApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\ClinicApiInterface;

class ClinicApi implements ClinicApiInterface
{

    // ...

    /**
     * Implementation of ClinicApiInterface#login
     */
    public function login($username, $password)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| The user name for login |
 **password** | **string**| The password for autorizate |

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **signUp**
> string signUp($body)

Create new user

Create new clinic employee

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/ClinicApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\ClinicApiInterface;

class ClinicApi implements ClinicApiInterface
{

    // ...

    /**
     * Implementation of ClinicApiInterface#signUp
     */
    public function signUp(User $body)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Swagger\Server\Model\User**](../Model/User.md)| Created user object |

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **updatePrivateOfiice**
> string updatePrivateOfiice($body)

Update user info

This can only be done by the logged in user.

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/ClinicApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\ClinicApiInterface;

class ClinicApi implements ClinicApiInterface
{

    // ...

    /**
     * Implementation of ClinicApiInterface#updatePrivateOfiice
     */
    public function updatePrivateOfiice(User $body)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Swagger\Server\Model\User**](../Model/User.md)|  |

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **uploadImg**
> Swagger\Server\Model\ApiResponse uploadImg($clinicId, $fileImg)

Upload img on main page clinic

This can only be done by the logged in user.

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/ClinicApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\ClinicApiInterface;

class ClinicApi implements ClinicApiInterface
{

    // ...

    /**
     * Implementation of ClinicApiInterface#uploadImg
     */
    public function uploadImg($clinicId, UploadedFile $fileImg = null)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clinicId** | **int**| ID of clinic to update |
 **fileImg** | **UploadedFile**| file to upload | [optional]

### Return type

[**Swagger\Server\Model\ApiResponse**](../Model/ApiResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

