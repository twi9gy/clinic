# Clinic

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**phone** | **string** |  | 
**email** | **string** |  | 
**address** | **string** |  | [optional] 
**desc** | **string** |  | 
**jobTime** | **string** |  | 
**urlImg** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


