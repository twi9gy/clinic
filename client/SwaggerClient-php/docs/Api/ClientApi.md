# Swagger\Client\ClientApi

All URIs are relative to *https://virtserver.swaggerhub.com/Clinic_T/Clinic_API/1.0.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createCommentClient**](ClientApi.md#createCommentClient) | **POST** /client/createComment | Create comment
[**editClient**](ClientApi.md#editClient) | **PUT** /client/EditInfo | Edit info user
[**entryClient**](ClientApi.md#entryClient) | **POST** /client/Entry | Entry with a doctor
[**getClinics**](ClientApi.md#getClinics) | **GET** /client/getClinic | Get Clinics
[**getDoctors**](ClientApi.md#getDoctors) | **GET** /client/getDoctors | Get doctors
[**logOutCleint**](ClientApi.md#logOutCleint) | **GET** /client/logout | LogOut Client
[**loginClient**](ClientApi.md#loginClient) | **GET** /client/login | Login Client
[**signUpClient**](ClientApi.md#signUpClient) | **POST** /client/signUp | Sign Up new Client


# **createCommentClient**
> createCommentClient($body)

Create comment

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ClientApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \stdClass; // object | 

try {
    $apiInstance->createCommentClient($body);
} catch (Exception $e) {
    echo 'Exception when calling ClientApi->createCommentClient: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | **object**|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **editClient**
> string editClient($body)

Edit info user

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ClientApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \stdClass; // object | 

try {
    $result = $apiInstance->editClient($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ClientApi->editClient: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | **object**|  |

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **entryClient**
> string entryClient($date, $body)

Entry with a doctor

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ClientApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$date = "date_example"; // string | 
$body = new \stdClass; // object | 

try {
    $result = $apiInstance->entryClient($date, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ClientApi->entryClient: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **date** | **string**|  |
 **body** | **object**|  |

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getClinics**
> \Swagger\Client\Model\Clinic[] getClinics($city, $type)

Get Clinics

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ClientApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$city = "city_example"; // string | 
$type = "type_example"; // string | 

try {
    $result = $apiInstance->getClinics($city, $type);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ClientApi->getClinics: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **city** | **string**|  |
 **type** | **string**|  |

### Return type

[**\Swagger\Client\Model\Clinic[]**](../Model/Clinic.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getDoctors**
> \Swagger\Client\Model\Employee[] getDoctors($filter, $body)

Get doctors

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ClientApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$filter = "filter_example"; // string | 
$body = new \stdClass; // object | 

try {
    $result = $apiInstance->getDoctors($filter, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ClientApi->getDoctors: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**|  |
 **body** | **object**|  |

### Return type

[**\Swagger\Client\Model\Employee[]**](../Model/Employee.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **logOutCleint**
> string logOutCleint()

LogOut Client

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ClientApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->logOutCleint();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ClientApi->logOutCleint: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **loginClient**
> string loginClient($username, $password)

Login Client

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ClientApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$username = "username_example"; // string | The client name for login
$password = "password_example"; // string | The password for autorizate

try {
    $result = $apiInstance->loginClient($username, $password);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ClientApi->loginClient: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| The client name for login |
 **password** | **string**| The password for autorizate |

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **signUpClient**
> \Swagger\Client\Model\Client signUpClient($body)

Sign Up new Client

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ClientApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \stdClass; // object | 

try {
    $result = $apiInstance->signUpClient($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ClientApi->signUpClient: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | **object**|  |

### Return type

[**\Swagger\Client\Model\Client**](../Model/Client.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

