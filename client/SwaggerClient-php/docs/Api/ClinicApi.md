# Swagger\Client\ClinicApi

All URIs are relative to *https://virtserver.swaggerhub.com/Clinic_T/Clinic_API/1.0.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**editInfo**](ClinicApi.md#editInfo) | **PUT** /clinic/editInfo | Eedit main info for clinic
[**logOutClinic**](ClinicApi.md#logOutClinic) | **GET** /clinic/logout | Logs out current logged in user session
[**login**](ClinicApi.md#login) | **GET** /clinic/login | Login user clinic
[**signUp**](ClinicApi.md#signUp) | **POST** /clinic/signUp | Create new user
[**updatePrivateOfiice**](ClinicApi.md#updatePrivateOfiice) | **PUT** /clinic/edit_privateOffice | Update user info
[**uploadImg**](ClinicApi.md#uploadImg) | **PUT** /clinic/uploadImg | Upload img on main page clinic


# **editInfo**
> string editInfo($token, $body)

Eedit main info for clinic

This can only be done by the logged in user.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ClinicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$token = "token_example"; // string | 
$body = new \Swagger\Client\Model\Clinic(); // \Swagger\Client\Model\Clinic | 

try {
    $result = $apiInstance->editInfo($token, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ClinicApi->editInfo: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **token** | **string**|  |
 **body** | [**\Swagger\Client\Model\Clinic**](../Model/Clinic.md)|  |

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **logOutClinic**
> string logOutClinic()

Logs out current logged in user session

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ClinicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->logOutClinic();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ClinicApi->logOutClinic: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **login**
> string login($username, $password)

Login user clinic

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ClinicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$username = "username_example"; // string | The user name for login
$password = "password_example"; // string | The password for autorizate

try {
    $result = $apiInstance->login($username, $password);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ClinicApi->login: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| The user name for login |
 **password** | **string**| The password for autorizate |

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **signUp**
> string signUp($body)

Create new user

Create new clinic employee

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ClinicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\User(); // \Swagger\Client\Model\User | Created user object

try {
    $result = $apiInstance->signUp($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ClinicApi->signUp: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\User**](../Model/User.md)| Created user object |

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updatePrivateOfiice**
> string updatePrivateOfiice($body)

Update user info

This can only be done by the logged in user.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ClinicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\User(); // \Swagger\Client\Model\User | 

try {
    $result = $apiInstance->updatePrivateOfiice($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ClinicApi->updatePrivateOfiice: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\User**](../Model/User.md)|  |

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **uploadImg**
> \Swagger\Client\Model\ApiResponse uploadImg($clinic_id, $file_img)

Upload img on main page clinic

This can only be done by the logged in user.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ClinicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$clinic_id = 789; // int | ID of clinic to update
$file_img = "/path/to/file.txt"; // \SplFileObject | file to upload

try {
    $result = $apiInstance->uploadImg($clinic_id, $file_img);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ClinicApi->uploadImg: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clinic_id** | **int**| ID of clinic to update |
 **file_img** | **\SplFileObject**| file to upload | [optional]

### Return type

[**\Swagger\Client\Model\ApiResponse**](../Model/ApiResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

