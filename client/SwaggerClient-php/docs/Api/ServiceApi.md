# Swagger\Client\ServiceApi

All URIs are relative to *https://virtserver.swaggerhub.com/Clinic_T/Clinic_API/1.0.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**changeService**](ServiceApi.md#changeService) | **PUT** /service/change | Change service
[**getService**](ServiceApi.md#getService) | **GET** /service/get | Get service


# **changeService**
> \Swagger\Client\Model\Service[] changeService($token, $body)

Change service

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ServiceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$token = "token_example"; // string | Token logged user
$body = array(new \Swagger\Client\Model\Service()); // \Swagger\Client\Model\Service[] | 

try {
    $result = $apiInstance->changeService($token, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ServiceApi->changeService: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **token** | **string**| Token logged user |
 **body** | [**\Swagger\Client\Model\Service[]**](../Model/Service.md)|  |

### Return type

[**\Swagger\Client\Model\Service[]**](../Model/Service.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getService**
> \Swagger\Client\Model\Service[] getService($token)

Get service

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ServiceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$token = "token_example"; // string | Token logged user

try {
    $result = $apiInstance->getService($token);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ServiceApi->getService: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **token** | **string**| Token logged user |

### Return type

[**\Swagger\Client\Model\Service[]**](../Model/Service.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

