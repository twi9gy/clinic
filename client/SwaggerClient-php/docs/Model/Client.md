# Client

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**name** | **string** |  | [optional] 
**login** | **string** |  | 
**password** | **string** |  | 
**email** | **string** |  | 
**phone** | **string** |  | [optional] 
**policy** | **string** |  | [optional] 
**pasport** | **string** |  | [optional] 
**belay** | **string** |  | [optional] 
**adress** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


